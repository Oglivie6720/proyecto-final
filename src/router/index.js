import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Registro from '../views/Registro.vue'
import Catalogo from '../views/Catalogo.vue'
import Anime from '../views/Anime.vue'
import Formulario from '../views/Formulario.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/registro',
    name: 'registro',
    component:Registro
  },
  {
    path:'/catalogo',
    name:'catalogo',
    component:Catalogo
  },
  {
    path:'/anime',
    name:'anime',
    component:Anime
  },
  {
    path:'/formulario',
    name:'formulario',
    component:Formulario
  }
]

const router = new VueRouter({
  routes
})

export default router

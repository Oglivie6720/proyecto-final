import {
    initializeApp
  } from 'firebase'
  
  const app = initializeApp({
    apiKey: "AIzaSyCoZETFfFZz933gkPNs-hXXtPYF1dvOT9Q",
    authDomain: "prog-graf-d91dc.firebaseapp.com",
    databaseURL: "https://prog-graf-d91dc.firebaseio.com",
    projectId: "prog-graf-d91dc",
    storageBucket: "prog-graf-d91dc.appspot.com",
    messagingSenderId: "995876539694",
    appId: "1:995876539694:web:9bf7799cf4aaae2fafbebf",
    measurementId: "G-6FP33PMVHM"
  });
  
  export const db = app.database();
  export const pfRef = db.ref('/Proyecto_final')
  